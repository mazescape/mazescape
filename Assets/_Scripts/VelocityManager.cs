﻿using UnityEngine;

namespace _Scripts
{
	public class VelocityManager : MonoBehaviour
	{
		private GameObject _hud;
		private int _averageVelocityCount;
		private const float OriginalWidth = 800f;
		private const float OriginalHeight = 600f;
		private Vector3 _scale;

		// Time print() style
		public GUIStyle Style;
		
		// Display score on GUI
		void OnGUI() { 
			_scale.x = Screen.width/OriginalWidth;
			_scale.y = Screen.height/OriginalHeight;
			_scale.z = 1;
			Matrix4x4 guiMatrix = GUI.matrix;
			GUI.matrix = Matrix4x4.TRS(new Vector3(_scale.x, _scale.y, 0), Quaternion.identity, _scale);
			GUI.Label(new Rect(-65f, 15f, 0, 0), "Average Speed: " + Mathf.Round(GamePoints.AverageVelocity * 100) / 100 + "ups", Style);
			GUI.matrix = guiMatrix;
		}

		public void RecalculateVerocity(float velocity)
		{
			GamePoints.AverageVelocity = ((GamePoints.AverageVelocity * _averageVelocityCount) + velocity) / (_averageVelocityCount + 1);
			_averageVelocityCount += 1;
		}
	}
}
