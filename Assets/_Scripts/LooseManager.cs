﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts
{
	public class LooseManager : MonoBehaviour {

		void Start()
		{
			Screen.lockCursor = false;	
		}
		
		public void BackToMenu()
		{
			SceneManager.LoadScene("Menu");
		}

		public void TryAgain()
		{
			SceneManager.LoadScene(ApplicationModel.currentScene);
		}
	}
}
