﻿using UnityEngine;

namespace _Scripts
{
	public class BunnyHopStreakManager : MonoBehaviour
	{
		//Sounds
		public AudioSource JumpSource;
		public AudioSource RampageSource;
		public AudioSource UnstoppableSource;
		public AudioSource DominatingSource;
		public AudioSource WickedSickSource;
		public AudioSource GodLikeSource;
		
		private float _previousVelocity = 0.0f;
		private int _velocityIncreaseCounter = 0;
		private string _streakDisplay;
		
		// Time print() style
		public GUIStyle Style;
		
		// Display streak on HUD
		void OnGUI() {
			if (_velocityIncreaseCounter > 0 && _streakDisplay != null)
			{
				GUI.Label(new Rect(Screen.width/2 - 150,Screen.height/2,300,100),_streakDisplay, Style);
			}
		}
		
		public void HandleJump(float currentMagnitude)
		{
			JumpSource.Play();
			var currentVelocity = Mathf.Round(currentMagnitude * 100) / 100;
			if (currentVelocity > _previousVelocity)
			{
				_velocityIncreaseCounter += 1;
				switch (_velocityIncreaseCounter)
				{
					case 3:
						RampageSource.Play();
						_streakDisplay = "Rampage!";
						break;
					case 4:
						UnstoppableSource.Play();
						_streakDisplay = "Unstoppable!";
						break;
					case 5:
						DominatingSource.Play();
						_streakDisplay = "Dominating!";
						break;
					case 6:
						GodLikeSource.Play();
						_streakDisplay = "God Like!";
						break;
				}
				if (_velocityIncreaseCounter == 7)
				{
					_streakDisplay = "Wicked Sick!";
					WickedSickSource.Play();
				}
				if (_velocityIncreaseCounter > 7)
				{
					WickedSickSource.Play();
					_streakDisplay = string.Format("Wicked Sick! x{0:0}", _velocityIncreaseCounter - 6);
				}
				GamePoints.BunnyHopStreakPoints += _velocityIncreaseCounter;
			}
			else
			{
				_velocityIncreaseCounter = 0;
				_streakDisplay = null;
			}
			_previousVelocity = currentVelocity;
		}
	}
}
