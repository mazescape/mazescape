﻿using System.Collections.Generic;

namespace _Scripts
{
    using System;

    public class GameSingleton
    {
        private static GameSingleton instance;
        private List<string> _levels = null;
        private int _currentLevel = 0;
        
        private GameSingleton()
        {
            _currentLevel = 0;
        }

        public string GetNextLevel()
        {
            if (_levels != null)
            {
                _currentLevel = (_currentLevel + 1) % _levels.Count;
                return _levels[_currentLevel];
            }
            return "Menu";
        }

        public string GetCurrentLevel()
        {
            return _levels[_currentLevel];
        }

        public void SetLevels(List<string> levels)
        {
            _levels = levels;
        }

        public bool HasLevels()
        {
            return _levels != null;
        }

        public static GameSingleton Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new GameSingleton();
                }
                return instance;
            }
        }
    }
}