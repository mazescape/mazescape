﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{

	public int _heaviness;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other)
	{
		if (Input.GetMouseButtonDown(1))
		{
			HandsController handsController =  other.gameObject.GetComponent<HandsController>();
            if(handsController.MaxWeightForPickup > _heaviness)
                handsController.box = this.gameObject;
		}
	}
}
