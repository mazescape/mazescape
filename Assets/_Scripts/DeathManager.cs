﻿using UnityEngine;

namespace _Scripts
{
	public class DeathManager : MonoBehaviour {
		
		private CharacterController _controller;
		private const float MaximumVelocity = 75.0f;
		public HealthManager HealthManager;

		private bool _died;
		
		// Use this for initialization
		void Start () {
			_controller = GetComponent<CharacterController>();
		}
	
		// Update is called once per frame
		void Update () {
			if (_controller.velocity.magnitude > MaximumVelocity)
			{
				if (!_died)
				{
					_died = true;
					HealthManager.Die();
				}
			}
		}
	}
}
