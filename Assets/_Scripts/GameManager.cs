﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts
{
	public class GameManager : MonoBehaviour
	{
		public string currentScene;
		public Transform PauseCanvas;
		public Transform Player;
		public List<String> Levels;
		
		private int _currentLevel = 0;

		void Start()
		{
			ApplicationModel.currentScene = currentScene;
			ResumeGame();
			GamePoints.ResetScore();
		}
		
		void Update()
		{
			if(Input.GetKeyDown(KeyCode.Escape)) 
			{
				if (!PauseCanvas.gameObject.activeInHierarchy)
				{
					PauseGame();
				}
				else 
				{
					ResumeGame();
				}
			} 
		}
		
		public void PauseGame()
		{
			PauseCanvas.gameObject.SetActive(true);
			Time.timeScale = 0;
			Player.GetComponent<BunnyHopController>().enabled = false;
			Screen.lockCursor = false;
		} 
		
		public void ResumeGame()
		{
			PauseCanvas.gameObject.SetActive(false);
			Time.timeScale = 1;
			Player.GetComponent<BunnyHopController>().enabled = true;
			Screen.lockCursor = true;
		}
		
		public void RestartLevel()
		{
			SceneManager.LoadScene(currentScene);
		}
		
		public void BackToMenu()
		{
			SceneManager.LoadScene("Menu");
		}
		
		public void QuitGame()
		{
			Application.Quit();
		}

		public void WinGame()
		{
			SceneManager.LoadScene("Win");
		}
		
		public void LooseGame()
		{
			SceneManager.LoadScene("Loose");
		}

	}
}
