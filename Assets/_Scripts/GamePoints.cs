﻿using UnityEngine;

namespace _Scripts
{
	public static class GamePoints
	{
		private const float TimeMultiplier = 5.0f;
		private const float GemsMultiplier = 20.0f;
		private const float AverageVelocityMultiplier = 5.0f;

		public static float Time { get; set; }
		
		public static float Gems { get; set; }

		public static float AverageVelocity { get; set; }
		
		public static float BunnyHopStreakPoints { get; set; }
		
		public static float HealthPoints { get; set; }

		public static void ResetScore()
		{
			Gems = 0.0f;
			Time = 0.0f;
			AverageVelocity = 0.0f;
			BunnyHopStreakPoints = 0.0f;
			HealthPoints = 100.0f;
		}

		public static float Score
		{
			get
			{
				return Gems*GemsMultiplier + AverageVelocity*AverageVelocityMultiplier + BunnyHopStreakPoints + HealthPoints - Time*TimeMultiplier;
			}
		}
	}
}
