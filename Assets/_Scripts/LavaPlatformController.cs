﻿using UnityEngine;
using _Scripts;

public class LavaPlatformController : MonoBehaviour
{
    private float _touchedTime;
    private bool _isDead;
    public HealthManager healthManager;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Collision();
        }
    }

    private void Collision()
    {
        if (_isDead)
        {
            return;
        }
        _touchedTime += Time.deltaTime;
        if (_touchedTime >= .01f)
        {
            _touchedTime = 0.0f;
            GamePoints.HealthPoints--;
            if (GamePoints.HealthPoints <= 0.0f)
            {
                _isDead = true;
                healthManager.Die();
            }
        }
    }
}