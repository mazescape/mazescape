﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts;

public class InitLevels : MonoBehaviour
{

	public List<string> Levels;
	private GameSingleton _gameSingleton = GameSingleton.Instance;

	// Use this for initialization
	void Start () {
		if (!_gameSingleton.HasLevels())
		{
			_gameSingleton.SetLevels(Levels);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
