﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts
{
	public class LevelManager : MonoBehaviour {

		
		private GameSingleton _gameSingleton = GameSingleton.Instance;
		public void LoadLevel()
		{
			SceneManager.LoadScene(_gameSingleton.GetCurrentLevel());
		}

		public void QuitGame()
		{
			Application.Quit();
		}
	}
}
