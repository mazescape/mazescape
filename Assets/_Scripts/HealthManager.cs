﻿using System;
using System.Collections;
using UnityEngine;

namespace _Scripts
{
	public class HealthManager : MonoBehaviour {

		public GameManager GameManager;
		public AudioSource DeathAudioSource;
		private GameObject _player;

		private void Start()
		{
			_player = GameObject.FindGameObjectWithTag("Player");
		}

		// Time print() style
		public GUIStyle Style;
		
		// Display time elapsed on GUI
		void OnGUI() {
			GUI.Label(new Rect(0,40,250,100), string.Format("Health: {0:0}", GamePoints.HealthPoints), Style);
		}

		public void UpdateHealth(float health)
		{
			GamePoints.HealthPoints += health;
			if (GamePoints.HealthPoints < 0)
			{
				Die();
			}
		}
		
		public void Die()
		{
			_player.transform.rotation = Quaternion.Euler(90, 0, 0);
			StartCoroutine(DieCorutine());
		}
		
		private IEnumerator DieCorutine()
		{
			DeathAudioSource.Play();
			yield return new WaitForSeconds(3);
			GameManager.LooseGame();
		}

	}
}
