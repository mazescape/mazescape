﻿using UnityEngine;

namespace _Scripts
{
    internal struct MovementCommand
    {
        public float MoveForward;
        public float MoveRight;
        public float MoveUp;
    }

    public class BunnyHopController : MonoBehaviour
    {
        //Camera settings
        public Transform PlayerView;
        public float PlayerViewYOffset = 1.0f;
        public float XMouseSensitivity = 30.0f;
        public float YMouseSensitivity = 30.0f;
        
        //Physics
        public float Gravity = 20.0f;
        public float Friction = 6;
    
        /* Movement stuff */
        public float MoveSpeed = 7.0f;                // Ground move speed
        public float RunAcceleration = 14.0f;         // Ground accel
        public float RunDeacceleration = 10.0f;       // Deacceleration that occurs when running on the ground
        public float AirAcceleration = 2.0f;          // Air accel
        public float AirDecceleration = 2.0f;         // Deacceleration experienced when ooposite strafing
        public float AirMovementControl = 0.3f;       // How precise air control is
        public float SideStrafeAcceleration = 50.0f;  // How fast acceleration occurs to get up to sideStrafeSpeed when
        public float SideStrafeSpeed = 1.0f;          // What the max speed to generate when side strafing
        public float JumpSpeed = 8.0f;                // The speed at which the character's up axis gains when hitting jump
        public float MoveScale = 1.0f;
    
        /*print() style */
        public GUIStyle Style;

        //public GameObject PickupObject;
        private HandsController _handsController;
        public GameObject PickupObject;
    
        /*FPS Stuff */
        public float FpsDisplayRate = 4.0f; // 4 updates per sec
        
        //Velocity measurement
        public VelocityManager VelocityManager;
        public BunnyHopStreakManager BunnyHopStreakManager;
    
        private int _frameCount = 0;
        private float _dt = 0.0f;
        private float _fps = 0.0f;
    
        private CharacterController _controller;
    
        // Camera rotations
        private float _rotX = 0.0f;
        private float _rotY = 0.0f;
    
        private Vector3 _moveDirectionNorm = Vector3.zero;
        private Vector3 _playerVelocity = Vector3.zero;
        private float _playerTopVelocity = 0.0f;
    
        // Q3: players can queue the next jump just before he hits the ground
        private bool _wishJump = false;
    
        // Used to display real time fricton values
        private float _playerFriction = 0.0f;
    
        // Player commands, stores wish commands that the player asks for (Forward, back, jump, etc)
        private MovementCommand _movementCommand;

        private void Start()
        {    
            // Put the camera inside the capsule collider
//            PlayerView.position = new Vector3(
//                transform.position.x,
//                transform.position.y + PlayerViewYOffset,
//                transform.position.z);
            PlayerView.position = Camera.main.gameObject.transform.position;
            _controller = GetComponent<CharacterController>();
            _handsController = PickupObject.GetComponent<HandsController>();
        }

        private void Update()
        {
            // Do FPS calculation
            _frameCount++;
            _dt += Time.deltaTime;
            if (_dt > 1.0 / FpsDisplayRate)
            {
                _fps = Mathf.Round(_frameCount / _dt);
                _frameCount = 0;
                _dt -= 1.0f / FpsDisplayRate;
                     }
            /* Ensure that the cursor is locked into the screen */
            if(Screen.lockCursor == false)
            {
                if(Input.GetMouseButtonDown(0))
                    Screen.lockCursor = true;
            }
    
            /* Camera rotation stuff, mouse controls this shit */
            _rotX -= Input.GetAxis("Mouse Y") * XMouseSensitivity * 0.02f;
            _rotY += Input.GetAxis("Mouse X") * YMouseSensitivity * 0.02f;
    
            // Clamp the X rotation
            if(_rotX < -90)
                _rotX = -90;
            else if(_rotX > 90)
                _rotX = 90;
    
            this.transform.rotation = Quaternion.Euler(0, _rotY, 0); // Rotates the collider
            PlayerView.rotation     = Quaternion.Euler(_rotX, _rotY, 0); // Rotates the camera
    
            
    
            /* Movement, here's the important part */
            QueueJump();
            if(_controller.isGrounded)
                GroundMove();
            else if(!_controller.isGrounded)
                AirMove();
    
            // Move the controller
            _controller.Move(_playerVelocity * Time.deltaTime);
    
            /* Calculate top velocity */
            if(_playerVelocity.magnitude > _playerTopVelocity)
                _playerTopVelocity = _playerVelocity.magnitude;
            
            //Calculate average velocity for GamePoints
            VelocityManager.RecalculateVerocity(_playerVelocity.magnitude);
            
    
            //Need to move the camera after the player has been moved because otherwise the camera will clip the player if going fast enough and will always be 1 frame behind.
            // Set the camera's position to the transform
//            PlayerView.position = new Vector3(
//                transform.position.x,
//                transform.position.y + PlayerViewYOffset,
//                transform.position.z);
            PlayerView.position = Camera.main.gameObject.transform.position;
        }

        /*******************************************************************************************************\
        |* MOVEMENT
        \*******************************************************************************************************/
    
        /**
         * Sets the movement direction based on player input
         */
        private void SetMovementDir()
        {
            _movementCommand.MoveForward = Input.GetAxis("Vertical");
            _movementCommand.MoveRight   = Input.GetAxis("Horizontal");
        }
    
        /**
         * Queues the next jump just like in Q3
         */
        private void QueueJump()
        {
            
            if(Input.GetKeyDown(KeyCode.Space) && !_wishJump && (!_handsController || _handsController.IsJumpPossible()))
                _wishJump = true;
            if(Input.GetKeyUp(KeyCode.Space))
                _wishJump = false;
        }
    
        /**
         * Execs when the player is in the air
        */
        private void AirMove()
        {
            Vector3 wishdir;
            var wishvel = AirAcceleration;
            float accel;
    
            var scale = CmdScale();
    
            SetMovementDir();
    
            wishdir =  new Vector3(_movementCommand.MoveRight, 0, _movementCommand.MoveForward);
            wishdir = transform.TransformDirection(wishdir);
    
            var wishspeed = wishdir.magnitude;
            wishspeed *= MoveSpeed;
    
            wishdir.Normalize();
            _moveDirectionNorm = wishdir;
            wishspeed *= scale;
    
            // CPM: Aircontrol
            var wishspeed2 = wishspeed;
            accel = Vector3.Dot(_playerVelocity, wishdir) < 0 ? AirDecceleration : AirAcceleration;
            // If the player is ONLY strafing left or right
            if(_movementCommand.MoveForward == 0 && _movementCommand.MoveRight != 0)
            {
                if(wishspeed > SideStrafeSpeed)
                    wishspeed = SideStrafeSpeed;
                accel = SideStrafeAcceleration;
            }
    
            Accelerate(wishdir, wishspeed, accel);
            if(AirMovementControl > 0)
                AirControl(wishdir, wishspeed2);
            // !CPM: Aircontrol
    
            // Apply gravity
            _playerVelocity.y -= Gravity * Time.deltaTime;
        }
    
        /**
         * Air control occurs when the player is in the air, it allows
         * players to move side to side much faster rather than being
         * 'sluggish' when it comes to cornering.
         */
        private void AirControl(Vector3 wishdir, float wishspeed)
        {
            float zspeed;
            float speed;
            float dot;
            float k;
            int i;
    
            // Can't control movement if not moving forward or backward
            if(Mathf.Abs(_movementCommand.MoveForward) < 0.001 || Mathf.Abs(wishspeed) < 0.001)
                return;
            zspeed = _playerVelocity.y;
            _playerVelocity.y = 0;
            /* Next two lines are equivalent to idTech's VectorNormalize() */
            speed = _playerVelocity.magnitude;
            _playerVelocity.Normalize();
    
            dot = Vector3.Dot(_playerVelocity, wishdir);
            k = 32;
            k *= AirMovementControl * dot * dot * Time.deltaTime;
    
            // Change direction while slowing down
            if (dot > 0)
            {
                _playerVelocity.x = _playerVelocity.x * speed + wishdir.x * k;
                _playerVelocity.y = _playerVelocity.y * speed + wishdir.y * k;
                _playerVelocity.z = _playerVelocity.z * speed + wishdir.z * k;
    
                _playerVelocity.Normalize();
                _moveDirectionNorm = _playerVelocity;
            }
    
            _playerVelocity.x *= speed;
            _playerVelocity.y = zspeed; // Note this line
            _playerVelocity.z *= speed;
        }
    
        /**
         * Called every frame when the engine detects that the player is on the ground
         */
        private void GroundMove()
        {
            Vector3 wishdir;
            Vector3 wishvel;
    
            // Do not apply friction if the player is queueing up the next jump
            ApplyFriction(!_wishJump ? 1.0f : 0);

            var scale = CmdScale();
    
            wishdir = new Vector3(_movementCommand.MoveRight, 0, _movementCommand.MoveForward);
            wishdir = transform.TransformDirection(wishdir);
            wishdir.Normalize();
            _moveDirectionNorm = wishdir;
    
            var wishspeed = wishdir.magnitude;
            wishspeed *= MoveSpeed;
            
            Accelerate(wishdir, wishspeed, RunAcceleration);
    
            // Reset the gravity velocity
            _playerVelocity.y = 0;

            if (!_wishJump) return;
            _playerVelocity.y = JumpSpeed;
            BunnyHopStreakManager.HandleJump(_controller.velocity.magnitude);
            _wishJump = false;
        }
    
        /**
         * Applies friction to the player, called in both the air and on the ground
         */
        private void ApplyFriction(float t)
        {
            var vec = _playerVelocity; // Equivalent to: VectorCopy();
            float vel;
            float speed;
            float newspeed;
            float drop;
    
            vec.y = 0.0f;
            speed = vec.magnitude;
            drop = 0.0f;
    
            /* Only if the player is on the ground then apply friction */
            if(_controller.isGrounded)
            {
                var control = speed < RunDeacceleration ? RunDeacceleration : speed;
                drop = control * Friction * Time.deltaTime * t;
            }
    
            newspeed = speed - drop;
            _playerFriction = newspeed;
            if(newspeed < 0)
                newspeed = 0;
            if(speed > 0)
                newspeed /= speed;
    
            _playerVelocity.x *= newspeed;
            // playerVelocity.y *= newspeed;
            _playerVelocity.z *= newspeed;
        }
    
        private void Accelerate(Vector3 wishdir, float wishspeed, float accel)
        {
            float addspeed;
            float accelspeed;
            float currentspeed;
    
            currentspeed = Vector3.Dot(_playerVelocity, wishdir);
            addspeed = wishspeed - currentspeed;
            if(addspeed <= 0)
                return;
            accelspeed = accel * Time.deltaTime * wishspeed;
            if(accelspeed > addspeed)
                accelspeed = addspeed;
    
            _playerVelocity.x += accelspeed * wishdir.x;
            _playerVelocity.z += accelspeed * wishdir.z;
        }
    
        private void OnGUI()
        {
            GUI.Label(new Rect(0, 0, 400, 100), "FPS: " + _fps, Style);
            var ups = _controller.velocity;
            ups.y = 0;
            GUI.Label(new Rect(0, 15, 400, 100), "Speed: " + Mathf.Round(ups.magnitude * 100) / 100 + "ups", Style);
            GUI.Label(new Rect(0, 30, 400, 100), "Top Speed: " + Mathf.Round(_playerTopVelocity * 100) / 100 + "ups", Style);
        }
    
        /*
        ============
        PM_CmdScale
        Returns the scale factor to apply to cmd movements
        This allows the clients to use axial -127 to 127 values for all directions
        without getting a sqrt(2) distortion in speed.
        ============
        */
        private float CmdScale()
        {
            int max;
            float total;
            float scale;
    
            max = (int)Mathf.Abs(_movementCommand.MoveForward);
            if(Mathf.Abs(_movementCommand.MoveRight) > max)
                max = (int)Mathf.Abs(_movementCommand.MoveRight);
            if(max <= 0)
                return 0;
    
            total = Mathf.Sqrt(_movementCommand.MoveForward * _movementCommand.MoveForward + _movementCommand.MoveRight * _movementCommand.MoveRight);
            scale = MoveSpeed * max / (MoveScale * total);
    
            return scale;
        }
    }
}
