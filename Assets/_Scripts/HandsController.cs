﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using _Scripts;

public class HandsController : MonoBehaviour
{

	public GameObject box;

	public int MaxWeightForJump = 50;
    public int MaxWeightForPickup = 80;
	
	private BoxManager _boxManager;
	private Rigidbody _hands;

	private float _rotY = 0;
	// Use this for initialization
	void Start ()
	{
		_hands = gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

		_rotY += Input.GetAxis("Mouse Y");
		Vector3 newVector = new Vector3(0,Math.Min(Math.Max(-35,_rotY), 45)/45+1f, 1.5f);
		this.transform.localPosition = newVector + new Vector3(0.5f, -0.3f, 0.3f);
		if (box!=null)
		{
			box.transform.position = this.transform.position;
			box.transform.eulerAngles = this.transform.parent.eulerAngles;

			if (Input.GetMouseButtonUp(1))
			{
				box = null;
			}
		}
		this.transform.localPosition = newVector;
	}

	public bool IsJumpPossible()
	{
		if (box != null)
            if(box.GetComponent<BoxManager>()._heaviness > MaxWeightForJump)
		{
			return false;
		}
		return true;
	}
}
