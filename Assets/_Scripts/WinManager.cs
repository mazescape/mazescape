﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Scripts
{
	public class WinManager : MonoBehaviour {
		
		// Time print() style
		public GUIStyle Style;
		private GameSingleton _gameSingleton = GameSingleton.Instance;

		void Start()
		{
			Screen.lockCursor = false;	
		}
		
		public void BackToMenu()
		{
			SceneManager.LoadScene("Menu");
		}

		public void NextLevel()
		{
			SceneManager.LoadScene(_gameSingleton.GetNextLevel());
		}

		public void TryAgain()
		{
			SceneManager.LoadScene(_gameSingleton.GetCurrentLevel());
		}
		
		// Display final scores on GUI
		void OnGUI() {
			var minutes = Mathf.FloorToInt(GamePoints.Time / 60F);
			var seconds = Mathf.FloorToInt(GamePoints.Time - minutes * 60);
			var niceTime = string.Format("Time elapsed: {0:0}:{1:00}", minutes, seconds);
			GUI.Label(new Rect(Screen.width/2 - 125,Screen.height/2 - 100,250,100), niceTime, Style);
			GUI.Label(new Rect(Screen.width/2 - 125,Screen.height/2 - 50,250,100), string.Format("Gems collected: {0:0}/10", GamePoints.Gems), Style);
			GUI.Label(new Rect(Screen.width/2 - 125,Screen.height/2,250,100), "Average Speed: " + Mathf.Round(GamePoints.AverageVelocity * 100) / 100 + "ups", Style);
			GUI.Label(new Rect(Screen.width/2 - 125,Screen.height/2 + 50,250,100), string.Format("Bunny Hop streak points: {0:0}", GamePoints.BunnyHopStreakPoints) , Style);
			GUI.Label(new Rect(Screen.width/2 - 125,Screen.height/2 + 120,250,100), string.Format("Overall score: {0:00}", GamePoints.Score), Style);
		}
	}
}
