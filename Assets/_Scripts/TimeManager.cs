﻿using UnityEngine;

namespace _Scripts
{
	public class TimeManager : MonoBehaviour {

		private bool _timeStarted;
		private bool _timeFinished;
		private const float OriginalWidth = 800f;
		private const float OriginalHeight = 600f;
		private Vector3 _scale;

		public GameManager GameManager;
		public Texture2D Texture;
		
		// Time print() style
		public GUIStyle Style;
		
		// Start timer on initialization
		void Start ()
		{
			StartTimer();
		}
	
		// Increase time by timedelta every frame
		void Update () {
			if (_timeFinished)
			{
				
			}
			else if (_timeStarted) {
				GamePoints.Time += Time.deltaTime;
			}   
		}
		
		// Display time elapsed on GUI
		void OnGUI() {
			var minutes = Mathf.FloorToInt(GamePoints.Time / 60F);
			var seconds = Mathf.FloorToInt(GamePoints.Time - minutes * 60);
			var niceTime = string.Format("Time: {0:0}:{1:00}", minutes, seconds);
			_scale.x = Screen.width/OriginalWidth;
			_scale.y = Screen.height/OriginalHeight;
			_scale.z = 1;
			Matrix4x4 guiMatrix = GUI.matrix;
			GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, _scale);
			GUI.DrawTexture(new Rect(0, 340, 365, 260), Texture);
			GUI.Label(new Rect(10, 32, 0, 0), niceTime, Style);
			GUI.matrix = guiMatrix;
		}

		public void StopTimer()
		{
			_timeFinished = true;
			GameManager.WinGame();
		}
		
		public void StartTimer()
		{
			_timeStarted = true;
		}
	}
}
