﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts
{
	public class GemManager : MonoBehaviour
	{
		private const float OriginalWidth = 800f;
		private const float OriginalHeight = 600f;
		private Vector3 _scale;
		// Time print() style
		public GUIStyle Style;

		public AudioSource GatherGemSource;
		
		// Display score on GUI
		void OnGUI()
		{
			Debug.Log("TUTAJ");
			_scale.x = Screen.width/OriginalWidth;
			_scale.y = Screen.height/OriginalHeight;
			_scale.z = 1;
			Matrix4x4 guiMatrix = GUI.matrix;
			GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, _scale);
			GUI.Label(new Rect(-65f,0, 0, 0), string.Format("Gems: {0:0}", GamePoints.Gems), Style);
			GUI.matrix = guiMatrix;
		}

		public void Score()
		{
			GatherGemSource.Play();
			GamePoints.Gems += 1;
		}
	}
}
