﻿using UnityEngine;

namespace _Scripts
{
	public class PointObject : MonoBehaviour {

		public GemManager GemManager;
		
		//when player gets a point
		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag("Player"))
			{
				GemManager.Score();
				Destroy(gameObject);
			}
		}
	}
}
