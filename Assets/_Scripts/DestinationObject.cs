﻿using UnityEngine;

namespace _Scripts
{
	public class DestinationObject : MonoBehaviour
	{
		public TimeManager TimeManager;
		
		//when player reaches the destination
		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag("Player"))
			{
				TimeManager.StopTimer();
			}
		}
	}
}
